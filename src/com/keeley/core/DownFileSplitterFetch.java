package com.keeley.core;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;

import org.eclipse.swt.widgets.Text;

import com.keeley.util.DownFileUtility;

/**
 * 下载上传子线程 . User: Administrator Date: 14-7-29 Time: 下午2:18 To change this
 * template use File | Settings | File Templates.
 */
public class DownFileSplitterFetch extends Thread {
	String sURL; // 下载文件的地址
	long nStartPos; // 文件分段的开始位置
	long nEndPos; // 文件分段的结束位置
	int nThreadID; // 线程的 ID
	boolean bDownOver = false; // 是否下载完成
	boolean bStop = false; // 停止下载
	DownFileAccess fileAccessI = null; // 文件对象
	boolean fileflag; // 是URL下载还是本地下载
	File file = null;// 本地下载文件
	boolean bFirst = true;
	Integer downid=null;

	/**
	 * 下载，上传子线程初始化
	 * 
	 * @param sURL
	 * @param sName
	 * @param nStart
	 * @param nEnd
	 * @param id
	 * @param fileflag
	 * @param downfile
	 * @throws IOException
	 */
	public DownFileSplitterFetch(String sURL, String sName, long nStart, long nEnd, int id, boolean fileflag,
			File downfile, boolean bFirst,int downid) throws IOException {
		this.sURL = sURL;
		this.downid=downid;
		this.nStartPos = nStart;
		this.nEndPos = nEnd;
		nThreadID = id;
		fileAccessI = new DownFileAccess(sName, nStartPos, bFirst);
		this.fileflag = fileflag;
		this.file = downfile;
		this.bFirst = bFirst;
	}

	/**
	 * 线程执行
	 */
	public void run() {
		if (fileflag) {
			this.urldownload();
		} else {
			this.filedownload();
		}
	}

	/**
	 * 打印回应的头信息
	 * 
	 * @param con
	 */
	public void logResponseHead(HttpURLConnection con) {
		for (int i = 1;; i++) {
			String header = con.getHeaderFieldKey(i);
			if (header != null) {
				DownFileUtility.log(downid,header + " : " + con.getHeaderField(header));
			} else {
				break;
			}
		}
	}

	/**
	 * 地址下载
	 */
	private void urldownload() {
		DownFileUtility.log(downid,"线程 " + nThreadID + " url 起始位置 >> " + nStartPos + "------结束位置 >> " + nEndPos);
		while (nStartPos < nEndPos && !bStop) {
			try {
				URL url = new URL(sURL);
				HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
				httpConnection.setRequestProperty("User-Agent", "NetFox");
				String sProperty = "bytes=" + nStartPos + "-";
				httpConnection.setRequestProperty("RANGE", sProperty); // 从起始开始读取
				DownFileUtility.log(downid,sProperty);
				InputStream input = httpConnection.getInputStream();
				byte[] b = new byte[1024];
				int nRead;
				while ((nRead = input.read(b, 0, 1024)) > 0 // 读取到结束点nEndPos
						&& nStartPos < nEndPos && !bStop) {
					if ((nStartPos + nRead) > nEndPos) {
						nRead = (int) (nEndPos - nStartPos);
					}
					nStartPos += fileAccessI.write(b, 0, nRead);
				}
				DownFileUtility.log(downid,"线程 " + nThreadID + " 下载起始位置 : " + nStartPos);
				fileAccessI.oSavedFile.close();
				DownFileUtility.log(downid,"线程 " + nThreadID + " 文件下载结束!");
				input.close();
				bDownOver = true;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (!bDownOver) {
			if (nStartPos >= nEndPos) {
				bDownOver = true;
			}
		}
	}

	/**
	 * 文件下载
	 */
	private void filedownload() {
		DownFileUtility.log(downid,"线程 " + nThreadID + " 需要下载的文件大小 " + (nEndPos - nStartPos));
		DownFileUtility.log(downid,"线程 " + nThreadID + " 起始位置 >> " + nStartPos + "------结束位置 >> " + nEndPos);
		while (nStartPos < nEndPos && !bStop) {
			try {
				RandomAccessFile input = new RandomAccessFile(file, "r");
				input.seek(nStartPos);
				byte[] b = new byte[1024];
				int nRead;
				while ((nRead = input.read(b, 0, 1024)) > 0 && nStartPos < nEndPos && !bStop) {
					if ((nStartPos + nRead) > nEndPos) {
						nRead = (int) (nEndPos - nStartPos);
					}
					nStartPos += fileAccessI.write(b, 0, nRead);
				}
				fileAccessI.oSavedFile.close();
				DownFileUtility.log(downid,"线程 " + nThreadID + " 下载结束!");
				input.close();
				bDownOver = true;
				input.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (!bDownOver) {
			if (nStartPos >= nEndPos) {
				bDownOver = true;
			}
		}
		DownFileUtility.log(downid,"线程 " + nThreadID + "最后起始位置 >> " + nStartPos);
	}

	/**
	 * 停止
	 */
	public void splitterStop() {
		bStop = true;
	}
	/**
	 * 停止
	 */
	public void splitterRestart() {
		bStop = false;
	}
}
